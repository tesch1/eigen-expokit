//  EXPOKIT is a set of user-friendly routines (in FORTRAN 77 and MATLAB)
//  aimed at computing matrix exponentials. More precisely, it computes
//  either a small matrix exponential in full, the action of a large
//  sparse matrix exponential on an operand vector, or the solution of a
//  system of linear ODEs with constant inhomogeneity. The backbone of
//  the sparse routines consists of Krylov subspace projection methods
//  (Arnoldi and Lanczos processes) and that is why the toolkit is capable
//  of coping with sparse matrices of very large dimension. The software
//  handles real and complex matrices and provides specific routines for
//  symmetric and Hermitian matrices. When dealing with Markov chains,
//  the computation of the matrix exponential is subject to probabilistic
//  constraints.  In addition to addressing general matrix exponentials,
//  a distinct attention is assigned to the computation of transient
//  states of Markov chains.
//
//  See http://www.maths.uq.edu.au/expokit
//
//  Notes:
//  ======
//  1. w(t) = exp(t*A)v is the analytic solution of the homogeneous
//     ODE problem:
//                 w' = Aw,  w(0) = v
//
//  2. w(t) = exp(t*A)*v + t*phi(t*A)u, where phi(z) = (exp(z)-1)/z,
//     is the analytic solution of the nonhomogeneous ODE problem:
//                 w' = Aw + u,  w(0) = v
//
//
//  The MATLAB version implements the following subset:
//
//  --- computational scripts:
//
//  chbv.m      Chebyshev algorithm for w = exp(t*A)*v
//  padm.m      irreducible Pade algorithm for exp(t*A)
//  phiv.m      Krylov algorithm for w = exp(t*A)*v + t*phi(t*A)*u
//  expv.m      Krylov algorithm for w = exp(t*A)*v
//  mexpv.m     Markov algorithm for w = exp(t*A)*v
//
//
//  --- utilities:
//
//  mat2ccs.m  save matrix into an ascii file under CCS format
//  mat2coo.m  save matrix into an ascii file under COO format
//  mat2crs.m  save matrix into an ascii file under CRS format
//
//  loadccs.m  load matrix from a file under CCS format
//  loadcoo.m  load matrix from a file under COO format
//  loadcrs.m  load matrix from a file under CRS format
//
//  example.m  simple illustrative example
//
//  Reference:
//  ==========
//  Roger B. Sidje (rbs@maths.uq.edu.au)
//  EXPOKIT: Software Package for Computing Matrix Exponentials.
//  ACM - Transactions On Mathematical Software, 24(1):130-156, 1998

//  Some useful stuff for c++.
//
//  Ported to C++/Eigen by Michael Tesch (tesch1@gmail.com), 2019.
//  https://gitlab.com/tesch1/eigen-expokit
//
#pragma once
#include <type_traits>
#include <utility>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/SparseLU>
#include <Eigen/SparseCholesky>
#include <Eigen/SparseQR>
#include <Eigen/IterativeLinearSolvers>

namespace eexpokit {

/// argument for the norm() function to request a fro-norm.
const static int Frobenius = -2;
extern Eigen::IOFormat OctaveFmt;
extern Eigen::IOFormat PythonFmt;

#ifndef EEXPOKIT_NODECL
Eigen::IOFormat OctaveFmt(Eigen::FullPrecision, Eigen::DontAlignCols, ", ", ";\n", "", "", "[", "]");
Eigen::IOFormat PythonFmt(Eigen::FullPrecision, 0, ", ", ",\n", "[", "]", "[", "]");
#endif

#include <type_traits>
template <class PlainObject>
struct expv_ret {
  PlainObject w;
  typename PlainObject::RealScalar err = 0;
  typename PlainObject::RealScalar hump = 0;
};

/// sign(um) function
template <typename T> int sign(T val)
{
  return (T(0) < val) - (val < T(0));
}

/// Check if a matrix is sparse or not
template <typename T, typename S = decltype(std::declval<T>().makeCompressed())> static std::true_type sparseTest(int);
template <typename T> static std::false_type sparseTest(...);
template <typename T, typename U = void> struct IsSparse : decltype(sparseTest<typename T::PlainObject>(0)) {};

/// Sparse matrices norm_1()
template <typename Derived>
typename Derived::RealScalar
norm_1(const Eigen::SparseMatrixBase<Derived> & A)
{
  typedef typename Derived::RealScalar Real;
  typedef typename Eigen::Matrix<Real,1,Eigen::Dynamic> RowVectorX;
  return (RowVectorX::Ones(A.rows()) * A.cwiseAbs()).maxCoeff();
}

/// Sparse matrices norm()
template <typename Derived>
typename Derived::RealScalar
norm_inf(const Eigen::SparseMatrixBase<Derived> & A)
{
  typedef typename Derived::RealScalar Real;
  typedef typename Eigen::Matrix<Real,Eigen::Dynamic,1> VectorX;
  return (A.cwiseAbs() * VectorX::Ones(A.cols())).maxCoeff();
}

/// Sparse matrices norm()
template <typename Derived>
typename Derived::RealScalar
norm(const Eigen::SparseMatrixBase<Derived> & A, int L = 2)
{
  typedef typename Derived::RealScalar Real;
  using namespace std;

  // vector norm
  if (A.rows() == 1 || A.cols() == 1 || L == Frobenius) {
    if (L == Frobenius || L == 2)
      return A.norm();
    Real v(0);
    for (Eigen::Index k = 0; k < A.outerSize(); ++k)
      for (typename Derived::InnerIterator it(A.derived(),k); it; ++it)
        //for (typename Eigen::SparseMatrix<Scalar>::InnerIterator it(A,k); it; ++it)
        v += pow(abs(it.value()), L);
    return pow(v, 1./L);
  }

  // matrix norm
  if (L == 2) {
    // no.
    throw std::invalid_argument("l2 matrix norm on sparse matrices not available.");
  }
  else if (L == 1) {
    // L1
    return norm_1(A);
  }
  else if (L == Eigen::Infinity) {
    // inf
    return norm_inf(A);
  }

  std::cout << "Unimplemented Sparse Norm, L=" << L << "\n";
  // punt, give them something
  return A.norm();
}

/// Dense matrix norm_inf()
template <typename DerivedA>
typename DerivedA::RealScalar
norm_1(const Eigen::MatrixBase<DerivedA> & A_)
{
  const DerivedA & A = A_.derived();
  return A.cwiseAbs().colwise().sum().maxCoeff();
}

/// Dense matrix norm_inf()
template <typename DerivedA>
typename DerivedA::RealScalar
norm_inf(const Eigen::MatrixBase<DerivedA> & A_)
{
  const DerivedA & A = A_.derived();
  return A.cwiseAbs().rowwise().sum().maxCoeff();
}

/// Dense matrix norm()
template <typename DerivedA>
typename DerivedA::RealScalar
norm(const Eigen::MatrixBase<DerivedA> & A_, int L = 2)
{
  const DerivedA & A = A_.derived();
  using namespace std;

  // vector norm
  if (A.rows() == 1 || A.cols() == 1 || L == Frobenius) {
    if (L == Frobenius || L == 2)
      return A.norm();
    return pow(A.array().abs().pow(L).sum(),1./L);
  }

  // matrix norm
  if (L == 2) {
    typedef typename std::conditional<Eigen::NumTraits<typename DerivedA::Scalar>::IsComplex,
                                      Eigen::ComplexEigenSolver<typename DerivedA::PlainObject>,
                                      Eigen::EigenSolver<typename DerivedA::PlainObject> >::type
      EigenSolverType;
    if (A.rows() == A.cols()) {
      EigenSolverType ed(A, false);
      return ed.eigenvalues().array().abs().maxCoeff();
    }
    else {
      EigenSolverType ed(A.transpose() * A, false);
      return ed.eigenvalues().array().abs().sqrt().maxCoeff();
    }
  }
  else if (L == 1) {
    // L1
    return norm_1(A_);
  }
  else if (L == Eigen::Infinity) {
    // inf
    return norm_inf(A_);
  }

  // punt
  throw std::invalid_argument("TODO.");
  return pow(A.array().abs().pow(L).sum(),1./L);
}

/// Sparse matlab-like left-division (D\N)
template <class DerivedD, class DerivedN,
          typename std::enable_if<IsSparse<DerivedD>::value,int>::type = 0>
typename DerivedN::PlainObject
Ldiv(const Eigen::EigenBase<DerivedD> & D_, const DerivedN & N)
{
  const DerivedD & D = D_.derived();
  typedef typename Eigen::SparseMatrixBase<DerivedD>::PlainObject MatrixT;

  // Pick one
  //typedef Eigen::SparseLU<MatrixC> SolverType;
  typedef Eigen::BiCGSTAB<MatrixT> SolverType; // *
  //typedef Eigen::LeastSquaresConjugateGradient<MatrixC> SolverType;
  //typedef Eigen::ConjugateGradient<MatrixC> SolverType;
  //typedef Eigen::SimplicialLDLT<MatrixC> SolverType;
  //typedef Eigen::SparseQR<MatrixC, Eigen::COLAMDOrdering<int> > SolverType;

  SolverType solver;
  solver.compute(D);
  if (solver.info() != Eigen::Success) {
    std::cout << "decomposition faild\n";
    return D;
  }

  auto R = solver.solve(N);

  if(solver.info() != Eigen::Success) {
    // solving failed
    std::cout << "solving faild\n";
  }
  return R;
}

/// Dense matrix Ldiv() (D\N)
template <typename DerivedD, typename DerivedN,
          typename std::enable_if<!IsSparse<DerivedD>::value,int>::type = 0>
typename DerivedN::PlainObject
Ldiv(const Eigen::EigenBase<DerivedD> & D_, const Eigen::EigenBase<DerivedN> & N_)
{
  const DerivedD & D = D_.derived();
  return D.derived().partialPivLu().solve(N_.derived());
}

namespace internal {
template <class D, class C> inline D unwrap(const Eigen::MatrixBase<C> & c) {
  return c.sparseView().template cast<typename D::Scalar>();
}
template <class D, class C> inline D unwrap(const Eigen::SparseMatrixBase<C> & c) {
  return c.template cast<typename D::Scalar>();;
}

template <class D, class C,
          typename std::enable_if<Eigen::NumTraits<typename D::Scalar>::IsComplex,int>::type = 0>
inline D maybe_real(const C & c) { return c; }
template <class D, class C,
          typename std::enable_if<!Eigen::NumTraits<typename D::Scalar>::IsComplex,int>::type = 0>
inline D maybe_real(const C & c) { return c.real(); }

}

}
