//  [w, err, hump] = expv( t, A, v, tol, m )
//  EXPV computes an approximation of w = exp(t*A)*v for a
//  general matrix A using Krylov subspace  projection techniques.
//  It does not compute the matrix exponential in isolation but instead,
//  it computes directly the action of the exponential operator on the
//  operand vector. This way of doing so allows for addressing large
//  sparse problems. The matrix under consideration interacts only
//  via matrix-vector products (matrix-free method).
//
//  w = expv( t, A, v )
//  computes w = exp(t*A)*v using a default tol = 1.0e-7 and m = 30.
//
//  [w, err] = expv( t, A, v )
//  renders an estimate of the error on the approximation.
//
//  [w, err] = expv( t, A, v, tol )
//  overrides default tolerance.
//
//  [w, err, hump] = expv( t, A, v, tol, m )
//  overrides default tolerance and dimension of the Krylov subspace,
//  and renders an approximation of the `hump'.
//
//  The hump is defined as:
//          hump = max||exp(sA)||, s in [0,t]  (or s in [t,0] if t < 0).
//  It is used as a measure of the conditioning of the matrix exponential
//  problem. The matrix exponential is well-conditioned if hump = 1,
//  whereas it is poorly-conditioned if hump >> 1. However the solution
//  can still be relatively fairly accurate even when the hump is large
//  (the hump is an upper bound), especially when the hump and
//  ||w(t)||/||v|| are of the same order of magnitude (further details in
//  reference below).
//
//  Example 1:
//  ----------
//    n = 100;
//    A = rand(n);
//    v = eye(n,1);
//    w = expv(1,A,v);
//
//  Example 2:
//  ----------
//    % generate a random sparse matrix
//    n = 100;
//    A = rand(n);
//    for j = 1:n
//        for i = 1:n
//            if rand < 0.5, A(i,j) = 0; end;
//        end;
//    end;
//    v = eye(n,1);
//    A = sparse(A); % invaluable for a large and sparse matrix.
//
//    tic
//    [w,err] = expv(1,A,v);
//    toc
//
//    disp('w(1:10) ='); disp(w(1:10));
//    disp('err =');     disp(err);
//
//    tic
//    w_matlab = expm(full(A))*v;
//    toc
//
//    disp('w_matlab(1:10) ='); disp(w_matlab(1:10));
//    gap = norm(w-w_matlab)/norm(w_matlab);
//    disp('||w-w_matlab|| / ||w_matlab|| ='); disp(gap);
//
//  In the above example, n could have been set to a larger value,
//  but the computation of w_matlab will be too long (feel free to
//  discard this computation).
//
//  See also MEXPV, EXPOKIT.

//  Roger B. Sidje (rbs@maths.uq.edu.au)
//  EXPOKIT: Software Package for Computing Matrix Exponentials.
//  ACM - Transactions On Mathematical Software, 24(1):130-156, 1998
//
//  Ported to C++/Eigen by Michael Tesch (tesch1@gmail.com), 2019.
//
#pragma once
#include "eexpokit/expokit.hpp"
#include <algorithm>
#include <Eigen/Dense>
#include <Eigen/SparseLU>
#include <Eigen/SparseCholesky>
#include <Eigen/SparseQR>
#include <Eigen/IterativeLinearSolvers>
#include <unsupported/Eigen/MatrixFunctions>

namespace eexpokit {

template <class tType, class DerivedA, class Derivedv,
          class Scalar = typename Eigen::ScalarBinaryOpTraits<typename DerivedA::Scalar,
                                                              typename Derivedv::Scalar>::ReturnType,
          class Real = typename Eigen::NumTraits<Scalar>::Real>
expv_ret<typename Derivedv::PlainObject>
expv(tType t,
     const Eigen::EigenBase<DerivedA> & A_,
     const Eigen::EigenBase<Derivedv> & v_,
     Real tol = 1.L/(1ull << (std::numeric_limits<Real>::digits / 2)),
     int m = 30)
{
  const DerivedA & A = A_.derived();
  const Derivedv & v = v_.derived();
  typedef typename Derivedv::PlainObject MatrixV;
  typedef Eigen::Matrix<Scalar,Eigen::Dynamic,1> DenseV;
  typedef Eigen::Matrix<Scalar,Eigen::Dynamic,Eigen::Dynamic> DenseA;
  using namespace std;

  expv_ret<MatrixV> ret;
  int n = A.rows();
  m = min(n,30);

  // check shape
  if (A.rows() != A.cols() || A.cols() != v.rows() || n == 0) {
    ret.err = std::numeric_limits<decltype(ret.err)>::infinity();
    ret.hump = 1;
    throw std::invalid_argument("Misshapen arguments to expv().");
  }

  if (n == 1) {
    // that's easy
    ret.err = 0;
    ret.hump = 1;
    ret.w = exp(A.coeff(0,0)) * v;
    return ret;
  }

  Real anorm = norm(A, Eigen::Infinity);
  int mxrej = 10;   Real btol  = 1.0e-7;
  Real gamma = 0.9; Real delta = 1.2;
  int mb    = m;    Real t_out   = abs(t);
  int nstep = 0;    Real t_new   = 0;
  Real t_now = 0;   Real s_error = 0;
  Real rndoff= anorm * std::numeric_limits<Real>::epsilon();
  Real normv = v.norm();

  if (anorm < tol || normv < tol) {
    if (normv < tol)
      ret.w = 0*v;
    else
      ret.w = v;
    ret.err = 0;
    ret.hump = 1;
    return ret;
  }

  //C---  obtain the very first stepsize ...
  int k1 = 2; Real xm = 1./m; Real beta = normv;
  Real fact = pow((m+1.)/exp(1.), (m+1))*sqrt(2*M_PI*(m+1));
  t_new = (1/anorm)*pow((fact*tol)/(4*beta*anorm), xm);
  Real s = pow(10., floor(log10(t_new))-1);
  t_new = ceil(t_new/s)*s;
  Real sgn = sign(t);
  nstep = 0;
  Real avnorm(0);

  DenseA w = v;
  Real hump = normv;
  int mx;
  Real err_loc = -1;
  //C---  step-by-step integration ...
  while (t_now < t_out) {
    nstep = nstep + 1;
    Real t_step = min( t_out-t_now,t_new );
    DenseA V(n,m+1);
    V.setZero();
    DenseA H(m+2,m+2);
    H.setZero();

    V.col(0) = (1./beta)*w;
    //C---  Arnoldi loop ...
    for (int j = 0; j < m; j++) {
      DenseV p = A*V.col(j);
      for (int i = 0; i <= j; i++) {
        H(i,j) = (V.col(i).adjoint()*p)(0,0);
        p = p - H(i,j) * V.col(i);
      }
      s = p.norm();
      if (s < btol) {
        k1 = 0;
        mb = j;
        t_step = t_out-t_now;
        break;
      }
      H(j+1,j) = s;
      V.col(j+1) = (1./s)*p;
    }
    if (k1 != 0) {
      H(m+1,m) = 1;
      avnorm = (A*V.col(m)).norm();
    }
    //C---  set 1 for the 2-corrected scheme ...
    int ireject = 0;
    DenseA F;
    //C---  loop while ireject<mxreject until the tolerance is reached ...
    while (ireject <= mxrej) {
      //C---  compute w = beta*V*exp(t_step*H)*e1 ..
      mx = mb + k1;
      F = (sgn*t_step*H.block(0,0,mx,mx)).exp();
      //C---  error estimate ...
      if (k1 == 0) {
        err_loc = btol;
        break;
      }
      else {
        Real phi1 = abs( beta*F(m+0,0) );
        Real phi2 = abs( beta*F(m+1,0) * avnorm );
        if (phi1 > 10*phi2) {
          err_loc = phi2;
          xm = 1./m;
        }
        else if (phi1 > phi2) {
          err_loc = (phi1*phi2)/(phi1-phi2);
          xm = 1./m;
        }
        else {
          err_loc = phi1;
          xm = 1./(m-1.);
        }
      }
      if (err_loc <= delta * t_step*tol) {
        break;
      }
      else {
        //C---  reject the step-size if the error is not acceptable ...
        t_step = gamma * t_step * pow(t_step*tol/err_loc,xm);
        s = pow(10., floor(log10(t_step)) - 1.);
        t_step = ceil(t_step/s) * s;
        if (ireject == mxrej) {
          std::cout << "The requested tolerance is too high: " << tol << "\n";
          std::cout << "err_loc=" << err_loc << " delta=" << delta << "\n";
          ret.err = std::numeric_limits<decltype(ret.err)>::infinity();
          ret.w = internal::unwrap<MatrixV>(w);
          return ret;
          throw std::invalid_argument("The requested tolerance is too high.");
        }
        ireject = ireject + 1;
      }
    }

    //C---  now update w = beta*V*exp(t_step*H)*e1 and the hump ...
    mx = mb + max( 0,k1-1 );
    w = V.block(0,0,V.rows(),mx)*(beta*F.block(0,0,mx,1));
    beta = w.norm();
    hump = max(hump,beta);

    //C---  update the time covered ...
    t_now = t_now + t_step;
    //C---  suggested value for the next stepsize ...
    t_new = gamma * t_step * pow(t_step*tol/err_loc,xm);
    s = pow(10., floor(log10(t_new))-1);
    t_new = ceil(t_new/s) * s;

    err_loc = max(err_loc,rndoff);
    s_error = s_error + err_loc;
  }
  hump = hump / normv;
  ret.err = s_error;
  ret.w = internal::unwrap<MatrixV>(w);
  ret.hump = hump;
  return ret;
}

}
