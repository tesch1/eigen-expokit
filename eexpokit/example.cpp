// MATLAB script illustrating the use of MEXPV.

// Roger B. Sidje (rbs@maths.uq.edu.au), 1996.
//
// Ported to c++ by Michael Tesch (tesch1@gmail.com), 2019.
//

#include <chrono>
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <unsupported/Eigen/MatrixFunctions>

#include "loadcrs.hpp"
#include "expv.hpp"
#include "mexpv.hpp"
#include "chbv.hpp"
#include "padm.hpp"

using namespace eexpokit;

class Timer
{
public:
  Timer() : beg_(clock_::now()) {}
  void reset() { beg_ = clock_::now(); }
  double elapsed() const {
    return std::chrono::duration_cast<second_>
      (clock_::now() - beg_).count(); }

private:
  typedef std::chrono::high_resolution_clock clock_;
  typedef std::chrono::duration<double, std::ratio<1> > second_;
  std::chrono::time_point<clock_> beg_;
};

template <class T>
void demo(const char * filename)
{
  typedef Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic> MatrixX;
  Timer timer;

  std::cout << "Loading the matrix ...\n";
  auto A = loadcrs<T>(filename);
  A = A; //  + T(0,2) * A;

  std::cout << "A(" << A.rows() << "," << A.cols() << ")\n";
  std::cout << "Computing w = exp(A)e_1 ...\n";
  auto n = A.rows();
  auto v = MatrixX::Identity(n,1);

  std::cout << "A(1:5) =" << (A.block(0,0,5,1)).transpose() << "\n";

  std::cout << " *** expv ***\n";
  timer.reset();
  auto ret = expv(1,A,v);
  std::cout << "time   =" << timer.elapsed() << " ";
  //std::cout << "w(1:5) =" << (ret.w.block(0,0,5,1)).transpose() << "\n";
  std::cout << "err    =" << ret.err << "\n";

  std::cout << " *** chbv ***\n";
  timer.reset();
  auto cy = chbv(A,v);
  std::cout << "time   =" << timer.elapsed() << " ";
  //std::cout << "y(1:5) =" << (cy.block(0,0,5,1)).transpose() << "\n";
  std::cout << "err    =" << (cy - ret.w).norm() << "\n";

  if (A.rows() < 513) {
    std::cout << " *** padm ***\n";
    timer.reset();
    auto E = padm(A);
    auto py = E*v;
    std::cout << "time   =" << timer.elapsed() << "\n";
    MatrixX F;
    timer.reset();
    F = A; F = F.exp();
    std::cout << "time   =" << timer.elapsed() << "\n";
    timer.reset();
    F = A; F = padm(F);
    std::cout << "time   =" << timer.elapsed() << " ";
    //std::cout << "y(1:5) =" << (py.block(0,0,5,1)).transpose() << "\n";
    std::cout << "err    =" << (py - ret.w).norm() << "\n";
    std::cout << "err2   =" << (E - F).norm() << "\n";
  }
  else
    std::cout << "-- Skipping padm --\n";

  //std::cout << " *** mexpv***\n";
  //ret = mexpv(1,A,v);
  //std::cout << "w(1:5) =" << (ret.w.block(0,0,5,1)).transpose() << "\n";
  //std::cout << "err =" << ret.err << "\n";
}

int main(int argc, char * argv[])
{
  const char * filename = "../expokit/data/c1024.crs";
  if (argc > 1)
    filename = argv[1];
#if 1
  std::cout << " *******************************************************************\n";
  std::cout << " float tol=" << 1.L/(1ull << (std::numeric_limits<float>::digits / 2)) << "\n";
  demo<float>(filename);
#endif
  std::cout << " *******************************************************************\n";
  std::cout << " complex float\n";
  demo<std::complex<float>>(filename);
#if 1
  std::cout << " *******************************************************************\n";
  std::cout << " double tol=" << 1.L/(1ull << (std::numeric_limits<double>::digits / 2)) << "\n";
  demo<double>(filename);
  std::cout << " *******************************************************************\n";
  std::cout << " complex double\n";
  demo<std::complex<double>>(filename);
#endif
}
