// A = loadcrs( 'filename.crs' )
//
// Load a matrix stored under the Compressed Row Storage (CRS) format by
// mat2crs.m
//
//
// Roger B. Sidje (rbs@maths.uq.edu.au), 1996.
//
//  Ported to c++ by Michael Tesch (tesch1@gmail.com), 2019.
//  https://gitlab.com/tesch1/eigen-expokit
//
#include <Eigen/Sparse>
#include <stdio.h>

namespace eexpokit {

template <class Scalar>
Eigen::SparseMatrix<Scalar>
loadcrs(const char * filename)
{
  auto fid = fopen(filename, "r");
  if (!fid) {
    perror("could not open file\n");
    Eigen::SparseMatrix<Scalar> A;
    return A;
  }
  int n;  fscanf(fid,"%u", &n);
  int nz; fscanf(fid,"%u", &nz);

  assert(nz >= n);

  Eigen::VectorXi ia(nz,1), ja(nz,1);
  Eigen::VectorXd ra(nz,1);
  ia.setZero();
  ja.setZero();
  ra.setZero();

  for (int i = 0; i <= n; i++) {
    fscanf(fid,"%u",&ia(i));
    ia(i)--;
  }

  for (int i = 0; i < nz; i++) {
    int x;
    double d;
    fscanf(fid,"%u",&x);
    ja(i) = x - 1;
    fscanf(fid,"%lf",&d);
    ra(i) = d;
  }
  fclose(fid);

  // expand row indices ...
  for (int i = n-1; i >= 0; i--) {
    for (int k = 0; k < ia(i+1)-ia(i); k++) {
      assert(nz > 0);
      ia(nz-1) = i;
      nz = nz-1;
    }
  }

  assert(nz == 1);

  Eigen::SparseMatrix<Scalar> A(n,n);
  for (int i = 0; i < ia.rows(); i++) {
    assert(ia(i) < n);
    assert(ja(i) < n);
    assert(ia(i) >= 0);
    assert(ja(i) >= 0);
    //if (i < 10 || i > ia.rows()-10)
    //  std::cout << "adding (" << ia(i) << "," << ja(i) << ") = " << ra(i) << "\n";
    A.insert(ia(i), ja(i)) = ra(i);
  }
  return A;
}

}
