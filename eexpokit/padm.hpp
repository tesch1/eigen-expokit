//  E = padm( A, p )
//  PADM computes the matrix exponential exp(A) using the irreducible
//  (p,p)-degree rational Pade approximation to the exponential function.
//
//  E = padm( A )
//  p is internally set to 6 (recommended and generally satisfactory).
//
//  See also CHBV, EXPOKIT and the MATLAB supplied functions EXPM and EXPM1.

//  Roger B. Sidje (rbs@maths.uq.edu.au)
//  EXPOKIT: Software Package for Computing Matrix Exponentials.
//  ACM - Transactions On Mathematical Software, 24(1):130-156, 1998
//
//  Ported to C++/Eigen by Michael Tesch (tesch1@gmail.com), 2019.
//  https://gitlab.com/tesch1/eigen-expokit
//
#pragma once
#include "eexpokit/expokit.hpp"
#include <algorithm>
#include <Eigen/Dense>

// TODO, FIX this A:
//A=[-0.444450578393624, -0.0452058962756795;
//   0.107939911590861, 0.257741849523849]

namespace eexpokit {

template <class DerivedA>
typename DerivedA::PlainObject
padm(const Eigen::EigenBase<DerivedA> & A_, int p = 6)
{
  const DerivedA & Au = A_.derived();
  typedef typename DerivedA::PlainObject::RealScalar Real;
  typedef typename DerivedA::Scalar Scalar;
  typedef typename DerivedA::PlainObject MatrixA;
  typedef Eigen::Matrix<Scalar,Eigen::Dynamic,1> VectorA;
  using namespace std;

  assert(Au.rows() == Au.cols());
  int n = Au.rows();

  // Pade coefficients (0-based instead of 0-based as in the literature)
  VectorA c(p+1,1);
  c(0) = 1;
  for (int k = 0; k < p; k++)
    c(k+1) = c(k)*Real((p+1.-(k+1.))/((k+1.)*(2.*p+1.-(k+1.))));

  // Scaling
  MatrixA A; // scaled
  Real s = norm(Au, Eigen::Infinity);
  if (s > 0.5) {
    s = max(0., trunc(log(s)/log(2.))+2.);
    A = pow(2.,-s)*Au;
  }
  else
    A = Au;

  // Horner evaluation of the irreducible fraction (see ref. above)

  MatrixA I_(n,n);
  I_.setIdentity();
  MatrixA A2 = A*A;
  MatrixA Q = c(p)*I_;
  MatrixA P = c(p-1)*I_;
  MatrixA E;
  int odd = 1;
  for (int k = p-1; k >= 0; k--) {
    if (odd == 1)
      Q = Q*A2 + c(k)*I_;
    else
      P = P*A2 + c(k)*I_;
    odd = 1-odd;
  }
  if (odd == 1) {
    Q = Q*A;
    Q = Q - P;
    E = -(I_ + 2*Ldiv(Q,P));
  }
  else {
    P = P*A;
    Q = Q - P;
    E = I_ + 2*Ldiv(Q,P);
  }

  // Squaring
  for (int k = 0; k < s; k++)
    E = E*E;

  return E;
}

}
