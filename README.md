# eigen-expokit

Port of `expokit` (https://www.maths.uq.edu.au/expokit/) to
header-file-only C++ routines using
[Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page).

# Expokit

This package provides Eigen implementations of some routines contained
in [EXPOKIT](http://www.maths.uq.edu.au/expokit). Those routines allow
an efficient calculation of the action of matrix exponentials on vectors
for large sparse matrices. For more details about the methods see
*R.B. Sidje, ACM Trans. Math. Softw., 24(1):130-156, 1998*
(or [its preprint](http://www.maths.uq.edu.au/expokit/paper.pdf)).

# Example

Currently only [chbv()](./eexpokit/chbv.hpp) and
[expv()](./eexpokit/expv.hpp) and [padm()](./eexpokit/padm.hpp) are
working.  [example.cpp](./eexpokit/example.cpp) has some examples of
using these functions, following the original `expokits` matlab
[example.m](./expokit/matlab/example.m).

The original `expokit` is [included](./expokit/)

# Quickstart (on a reasonable unix system)

```sh
git clone https://gitlab.com/tesch1/eigen-expokit.git
cd eigen-expokit
cmake -Bbuild .
cmake --build build
cd build
./example
```

# What it is

relevant snippet from the `expokit` [README](./expokit/README) :

```
//  chbv.m      Chebyshev algorithm for w = exp(t*A)*v
//  padm.m      irreducible Pade algorithm for exp(t*A)
//  phiv.m      Krylov algorithm for w = exp(t*A)*v + t*phi(t*A)*u
//  expv.m      Krylov algorithm for w = exp(t*A)*v
//  mexpv.m     Markov algorithm for w = exp(t*A)*v
```

# License

I suppose this derivative is covered by the fantastic [original
notice](./expokit/copyright):

```
                              NOTICE

Permission to use, copy, modify, and distribute EXPOKIT and its
supporting documentation for non-commercial purposes, is hereby
granted without fee, provided that this permission message and
copyright notice appear in all copies. Approval must be sought for
commercial purposes as testimony of its usage in applications.

Neither the Institution (University of Queensland) nor the Author
make any representations about the suitability of this software for
any purpose.  This software is provided ``as is'' without express or
implied warranty.

The work resulting from EXPOKIT has been published in ACM-Transactions 
on Mathematical Software, 24(1):130-156, 1998.

The bibtex record of the citation:

@ARTICLE{EXPOKIT,
        AUTHOR  = {Sidje, R. B.},
        TITLE   = {{\sc Expokit.} {A} Software Package for
		  Computing Matrix Exponentials},
        JOURNAL = {ACM Trans. Math. Softw.},
        VOLUME  = {24},
        NUMBER  = {1},
        PAGES   = {130-156}
        YEAR    = {1998}
}

Certain elements of the current software may include inadequacies
that may be corrected at any time, as they are discovered. The Web 
always contains the latest updates.

                            Original Author:
                 Roger B. Sidje <rbs@maths.uq.edu.au>
           Department of Mathematics, University of Queensland 
     Brisbane, QLD-4072, Australia, (c) 1996-2006 All Rights Reserved
```
