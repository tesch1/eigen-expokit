%
% MATLAB script illustrating the use of MEXPV.
%
oldpath = path;
path(oldpath,'../../higham-expmv');

disp('Loading the matrix ...');
if 1,
    A = loadcrs('../data/c1024.crs');
    b = eye(n,1);
elseif 1
    A = -gallery('poisson',10);
    b = linspace(-1,1,size(A,1))';
end

A = A + 2i*A;
v = b;
t = 100;
t = 1;

[n,n] = size(A);
disp(sprintf('size(A) = %d x %d',n,n));
disp(sprintf('Computing w = exp(A)e_1 ... t=%f',t));
err = 0;

%  chbv.m      Chebyshev algorithm for w = exp(t*A)*v
%  padm.m      irreducible Pade algorithm for exp(t*A)
%  phiv.m      Krylov algorithm for w = exp(t*A)*v + t*phi(t*A)*u
%  expv.m      Krylov algorithm for w = exp(t*A)*v
%  mexpv.m     Markov algorithm for w = exp(t*A)*v

if 0,
disp(' ***|padm |*** *** *** *** *** *** *** *** *** *** *** *** ');
tic
[wp] = padm(t*A)*v;
toc
disp('w(1:10) =');
disp(w(1:10).');
disp('err =');
disp(norm(w - wp))
end

disp(' ***|phiv |*** *** *** *** *** *** *** *** *** *** *** *** ');
%  [w, err] = phiv( t, A, u, v, tol, m )
tic
[w,err] = phiv(t,A,0*v,v);
toc
disp('w(1:10) =');
disp(w(1:10).');
disp('err =');
disp(err)

disp(' ***|chbv |*** *** *** *** *** *** *** *** *** *** *** *** ');
tic
[w2] = chbv(t*A,v);
toc
disp('w(1:10) =');
disp(w2(1:10).');
disp('err =');
disp(norm(w-w2))

disp(' ***|expv |*** *** *** *** *** *** *** *** *** *** *** *** ');
tic
[w,err,hump] = expv(t,A,v);
toc
disp('w(1:10) =');
disp(w(1:10).');
disp('err =');
disp(err)

disp(' ***|mexpv|*** *** *** *** *** *** *** *** *** *** *** *** ');
tic
[w,err] = mexpv(t,A,v);
toc
disp('w(1:10) =');
disp(w(1:10).');
disp('err =');
disp(err)

disp(' ***|expmv|*** *** *** *** *** *** *** *** *** *** *** *** ');
%function [f,s,m,mv,mvd,unA] = ...
%          expmv(t,A,b,M,prec,shift,bal,full_term,prnt)
tic
[w2,err] = expmv(t,A,v,[],[]);
toc
disp('w(1:10) =');
disp(w2(1:10).');
disp('err =');
disp(norm(w-w2))


path(oldpath);
